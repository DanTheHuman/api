<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm; ?>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <div id="app">
        <button v-if="subscribed" v-on:click="subscribe">Unsubscribe</button>
        <button v-else v-on:click="subscribe">Subscribe</button>
    </div>
<?php
//$js = <<<JS
// $('form').on('beforeSubmit', function(){
// alert('Работает!');
// return false;
// });
//JS;
//
//$js = <<<JS
// $('form').on('beforeSubmit', function(){
// var data = $(this).serialize();
// $.ajax({
// url: 'index.php?r=test%2Fpage',
// type: 'POST',
// data: data,
// success: function(res){
// console.log(res);
// },
// error: function(){
// alert('Error!');
// }
// });
// return false;
// });
//JS;
//$this->registerJs($js);
//?>
<?php $js = <<<JS
new Vue({
  el: "#app",
  data: {
    subscribed: false
  },
  methods: {
  	subscribe: function(){
    	this.subscribed = !this.subscribed
        axios.post('index.php?r=test%2Fpage', {
        subscribe: this.subscribed
      })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
    }
  }
})
JS;
Yii::$app->view->registerJs($js, 4);
