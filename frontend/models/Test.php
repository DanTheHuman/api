<?php


namespace frontend\models;


class Test
{
    public static function tableName()
    {
        return 'subscribe';
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['subscribe', 'safe']
        ];
    }
}