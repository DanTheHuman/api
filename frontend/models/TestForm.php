<?php


namespace frontend\models;


use app\models\Subscribe;
use common\models\User;
use yii\base\Model;

class TestForm extends Model
{
    public $subscribe;

    public function tableName(){
        return 'subscribe';
    }

    public function rules()
    {
        return [
          ['subscribe','safe'],
        ];
    }

    public function formName()
    {
        return '';
    }

     public function toogleSubscribe()
    {
        /** @var User $user */
        $user = \Yii::$app->user->identity;
        /** @var Subscribe $subscibe */
        if ($subscibe = $user->getSubscibe()->one()) {
            $subscibe->subscribe = (int)!$subscibe->subscribe;
            $subscibe->update();
        } else {
            $subscibe = new Subscribe(['user_id' => $user->id]);

                $subscibe->subscribe = 1;
                $subscibe->save();
        }


    }

}