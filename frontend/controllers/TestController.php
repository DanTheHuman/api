<?php


namespace frontend\controllers;
use frontend\models\Test;
use frontend\models\TestForm;
use Yii;
use yii\base\Controller;


class TestController extends Controller
{
    public function actionPage()
    {
        $form_model = new TestForm();
        $form_model->toogleSubscribe();

        if(\Yii::$app->request->isAjax){
            return 'Запрос принят!';
        }
        $form_model->subscribe = false;
        return $this->render('page', compact('form_model'));
    }

}