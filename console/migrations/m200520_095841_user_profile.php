<?php

use yii\db\Migration;

/**
 * Class m200520_095841_user_profile
 */
class m200520_095841_user_profile extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'description', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200520_095841_user_profile cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200520_095841_user_profile cannot be reverted.\n";

        return false;
    }
    */
}
