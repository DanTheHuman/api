<?php

use yii\db\Migration;

/**
 * Class m200519_093728_create_subscribe
 */
class m200519_093728_create_subscribe extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('subscribe', [
            'id' => $this->integer(),
            'name' => $this->string(),
            'subscribe' => $this->boolean()->defaultValue(false),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200519_093728_create_subscribe cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200519_093728_create_subscribe cannot be reverted.\n";

        return false;
    }
    */
}
